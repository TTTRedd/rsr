class CfgFunctions 
{
	class rsr_vanilla 
	{
		tag = "RsR";

		class init 
		{
			file="rsr\rsr_vanilla\functions";

			class advancedTowingInit {postInit = 1;};
			class remove_turret_weapons {};
			class fired {};
			
		};
	};
};
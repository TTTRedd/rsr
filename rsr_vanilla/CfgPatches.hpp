class CfgPatches {
	class rsr_vanilla {
		requiredAddons[] = {"rsr_core"};
		//requiredVersion = 0.1;
		units[] = {

			"rsr_panzerhaubitze_flecktarn",
			"rsr_panzerhaubitze_tropentarn",

			"rsr_fennek_transport_flecktarn", 
			"rsr_fennek_hmg_flecktarn", 
			"rsr_fennek_gmg_flecktarn", 

			"rsr_fennek_transport_tropentarn", 
			"rsr_fennek_hmg_tropentarn", 
			"rsr_fennek_gmg_tropentarn", 
			
			"rsr_bergepanzer_tropentarn",
			"rsr_bergepanzer_flecktarn",

			//hidden ingame and obsolete
			"rsr_wisent_transport_flecktarn", 
			"rsr_wisent_covered_flecktarn", 
			"rsr_wisent_repair_flecktarn", 
			"rsr_wisent_ammo_flecktarn", 
			"rsr_wisent_fuel_flecktarn", 
			"rsr_wisent_medical_flecktarn", 
			"rsr_gepard_flecktarn",
			"rsr_wisent_transport_tropentarn", 
			"rsr_wisent_covered_tropentarn", 
			"rsr_wisent_repair_tropentarn", 
			"rsr_wisent_ammo_tropentarn", 
			"rsr_wisent_fuel_tropentarn", 
			"rsr_wisent_medical_tropentarn", 
			"rsr_gepard_tropentarn"
			//hidden ingame and obsolete
			
			};
		weapons[] = {};
	};
};
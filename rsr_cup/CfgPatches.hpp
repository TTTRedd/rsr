class CfgPatches {
	class rsr_cup {
		requiredAddons[] = {"rsr_core"};
		//requiredVersion = 0.1;
		units[] = 
		{
			"rsr_c_161",
			"rsr_c_161_viv",
			"rnt_bw_wintertarn_uniform_item",
            "rnt_bw_wintertarn_helm_item",

			//hidden ingame and obsolete
			"rsr_boxer_transport_flecktarn", 
			"rsr_boxer_medical_flecktarn",
			"rsr_boxer_transport_tropentarn",
			"rsr_boxer_medical_tropentarn"
			//hidden ingame and obsolete
		};
		weapons[] =
		{
			"rnt_bw_wintertarn_uniform"
		};
	};
};


class CfgWeapons
{
	//Wintertarn Uniform
	class InventoryItem_Base_F;
	class ItemCore;
	class CUP_U_O_CHDKZ_Kam_01;
	class CUP_H_USMC_LWH_DES;

	class UniformItem: InventoryItem_Base_F{
		type=801;
	};
	class rnt_bw_wintertarn_uniform_item: CUP_U_O_CHDKZ_Kam_01{
		author="rnt";
		displayName="Wintertarn Ueberzug";
		picture="\rsr\rsr_cup\data\wintertarn\rnt_winter_uniform_ui_pre_ca";
		model="\A3\Characters_F\Common\Suitpacks\suitpack_universal_F.p3d";

		class ItemInfo: UniformItem{
			uniformModel="-";
			uniformClass="rnt_bw_wintertarn_uniform";
			containerClass="Supply40";
			mass=15;
		};
	};
	class rnt_bw_wintertarn_helm_item: CUP_H_USMC_LWH_DES{
		author="rnt";
		displayName="Wintertarn Ueberzug Helm";
		picture="\rsr\rsr_cup\data\wintertarn\rnt_winter_helm_ui_pre_ca";
		hiddenSelectionsTextures[]={
			"\rsr\rsr_cup\data\wintertarn\rnt_winter_helm_co"
		};
	};
};


class CfgVehicles 
{
	//Wintertarn Uniform
	class CUP_Creatures_Military_CHDKZ_Soldier_04;
	class rnt_bw_wintertarn_uniform: CUP_Creatures_Military_CHDKZ_Soldier_04
	{
		author="rnt";
		uniformClass="rnt_bw_wintertarn_uniform_item";
		hiddenSelectionsTextures[]={
			"\rsr\rsr_cup\data\wintertarn\rnt_winter_jacke_co",
			"\rsr\rsr_cup\data\wintertarn\rnt_winter_hose_co"
		};
	};

	//C-161 Transport
	class CUP_B_C130J_USMC;
	class rsr_c_161 : CUP_B_C130J_USMC 
	{
		author = "RsR";
		displayName = "C-161 Transall 2";
		editorPreview = "";
		editorCategory = "rsrVehicles";
		editorSubcategory = "rsrPlane";

		hiddenSelectionsTextures[] = {
			"\rsr\rsr_cup\data\c_161\redd_c_161_body_co.paa", 
			"\rsr\rsr_cup\data\c_161\redd_c_161_wings_co.paa"
		};
	};
	//C-161 VIV
	class CUP_B_C130J_Cargo_USMC;
	class rsr_c_161_viv : CUP_B_C130J_Cargo_USMC 
	{
		author = "RsR";
		displayName = "C-161 Transall 2 (VIV)";
		editorPreview = "";
		editorCategory = "rsrVehicles";
		editorSubcategory = "rsrPlane";

		hiddenSelectionsTextures[] = 
		{
			"\rsr\rsr_cup\data\c_161\redd_c_161_body_co.paa", 
			"\rsr\rsr_cup\data\c_161\redd_c_161_wings_co.paa"
		};
	};

	//
	//Vehicles below all hidden ingame and obsolete
	//

	//Boxer-Transport
	class CUP_B_M1126_ICV_M2_Desert;
	class rsr_boxer_transport_flecktarn : CUP_B_M1126_ICV_M2_Desert 
	{
		author = "RsR";
		displayName = "GTK Boxer - Transport";
		typicalCargo[] = {"B_Soldier_F"};
		side = 1;
		crew = "B_Soldier_F";
		editorPreview = "";
		editorCategory = "rsrFleck";
		editorSubcategory = "rsrAPC";

		//hide ingame, vehicles no longe needed
		scope=1;
        scopeCurator = 1;
		scopeArsenal = 1;

		class TransportItems {};
		class TransportWeapons {};
		class TransportMagazines {};

		hiddenSelectionsTextures[] = 
		{
			"rsr\rsr_cup\data\boxer\boxer_flecktarn_body1_co.paa", 
			"rsr\rsr_cup\data\boxer\boxer_flecktarn_body2_co.paa", 
			"rsr\rsr_cup\data\boxer\boxer_flecktarn_body2_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\icv_des_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\stryker_alfa_des_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\stryker_alfa_des_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\slat_armor_des_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\slat_armor_des_co.paa"
		};

		class TextureSources 
		{
			class boxer_flecktarn 
			{
				displayName = "Boxer Flecktarn";
				author = "Redd";
				factions[] = {"BLU_F"};

				textures[] = 
				{
					"rsr\rsr_cup\data\boxer\boxer_flecktarn_body1_co.paa", 
					"rsr\rsr_cup\data\boxer\boxer_flecktarn_body2_co.paa", 
					"rsr\rsr_cup\data\boxer\boxer_flecktarn_body2_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\icv_des_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\stryker_alfa_des_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\stryker_alfa_des_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\slat_armor_des_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\slat_armor_des_co.paa"
				};
			};

			class boxer_tropentarn 
			{
				displayName = "Boxer Tropentarn";
				author = "Redd";
				factions[] = {"BLU_F"};

				textures[] = 
				{
					"rsr\rsr_cup\data\boxer\boxer_tropentarn_body1_co.paa", 
					"rsr\rsr\rsr_cup\data\boxer\boxer_tropentarn_body2_co.paa", 
					"rsr\rsr\rsr_cup\data\boxer\boxer_tropentarn_body2_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\icv_des_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\stryker_alfa_des_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\stryker_alfa_des_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\slat_armor_des_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\slat_armor_des_co.paa"
				};
			};
		};
	};
	class rsr_boxer_transport_tropentarn : rsr_boxer_transport_flecktarn 
	{
		displayName = "GTK Boxer - Transport";
		editorCategory = "rsrTrope";
		editorSubcategory = "rsrAPC";

		hiddenSelectionsTextures[] = 
		{
			"rsr\rsr_cup\data\boxer\boxer_tropentarn_body1_co.paa", 
			"rsr\rsr_cup\data\boxer\boxer_tropentarn_body2_co.paa", 
			"rsr\rsr_cup\data\boxer\boxer_tropentarn_body2_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\icv_des_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\stryker_alfa_des_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\stryker_alfa_des_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\slat_armor_des_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\slat_armor_des_co.paa"
		};
	};

	//Boxer Medic
	class CUP_B_M1133_MEV_Desert;
	class rsr_boxer_medical_flecktarn : CUP_B_M1133_MEV_Desert 
	{
		author = "RsR";
		displayName = "GTK Boxer - Lazarett";
		typicalCargo[] = {"B_Soldier_F"};
		side = 1;
		crew = "B_Soldier_F";
		editorPreview = "";
		editorCategory = "rsrFleck";
		editorSubcategory = "rsrAPC";

		//hide ingame, vehicles no longe needed
		scope=1;
        scopeCurator = 1;
		scopeArsenal = 1;

		class TransportItems {};
		class TransportWeapons {};
		class TransportMagazines {};

		hiddenSelectionsTextures[] = 
		{
			"rsr\rsr_cup\data\boxer\boxer_flecktarn_body1_co.paa", 
			"rsr\rsr_cup\data\boxer\boxer_flecktarn_body2_co.paa", 
			"rsr\rsr_cup\data\boxer\boxer_flecktarn_medevac_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\slat_armor_des_co.paa"
		};
		
		class TextureSources 
		{
			class boxer_flecktarn 
			{
				displayName = "Boxer Flecktarn";
				author = "Redd";
				factions[] = {"BLU_F"};

				textures[] = 
				{
					"rsr\rsr_cup\data\boxer\boxer_flecktarn_body1_co.paa", 
					"rsr\rsr_cup\data\boxer\boxer_flecktarn_body2_co.paa", 
					"rsr\rsr_cup\data\boxer\boxer_flecktarn_medevac_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\Woodland\slat_armor_des_co.paa"
				};
			};

			class boxer_tropentarn 
			{
				displayName = "Boxer Tropentarn";
				author = "Redd";
				factions[] = {"BLU_F"};

				textures[] = 
				{
					"rsr\rsr_cup\data\boxer\boxer_tropentarn_body1_co.paa", 
					"rsr\rsr_cup\data\boxer\boxer_tropentarn_body2_co.paa", 
					"rsr\rsr_cup\data\boxer\boxer_tropentarn_medevac_co.paa", 
					"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\slat_armor_des_co.paa"
				};
			};
		};
	};
	class rsr_boxer_medical_tropentarn : rsr_boxer_medical_flecktarn 
	{
		displayName = "GTK Boxer - Lazarett";
		editorCategory = "rsrTrope";
		editorSubcategory = "rsrAPC";

		hiddenSelectionsTextures[] = 
		{
			"rsr\rsr_cup\data\boxer\boxer_tropentarn_body1_co.paa", 
			"rsr\rsr_cup\data\boxer\boxer_tropentarn_body2_co.paa", 
			"rsr\rsr_cup\data\boxer\boxer_tropentarn_medevac_co.paa", 
			"cup\wheeledvehicles\cup_wheeledvehicles_stryker\data\slat_armor_des_co.paa"
		};
	};
};
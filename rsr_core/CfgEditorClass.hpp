class CfgEditorCategories
{
    class rsrVehicles
    {
        displayName = "RSR Vehicles";
    };
};

class CfgEditorSubcategories
{
    class rsrArti
    {       
        displayName = "Artillerie";   
    };
    class rsrCar
    {       
        displayName = "Auto";   
    };
    class rsrPlane
    {       
        displayName = "Flugzeug";   
    };
    class rsrAPC
    {       
        displayName = "Truppentransporter";   
    };
    class rsrTank
    {       
        displayName = "Panzer";   
    };
};
class CfgVehicleClasses {
	class RSR_vehicleClass_flecktarn {
		displayName = "RSR Units (Flecktarn)"; 		//In-game name
		priority = 1; 								// How far down it is on the menu
		scope = 1;
	};
	
	class RSR_vehicleClass_tropentarn {
		displayName = "RSR Units (Tropentarn)"; 	//In-game name
		priority = 2; 								// How far down it is on the menu
		scope = 1;
	};
};
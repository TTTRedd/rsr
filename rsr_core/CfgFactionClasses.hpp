class CfgFactionClasses {
	class RSR_faction_flecktarn {
		icon = "\rsr\rsr_core\data\ui\rsr_logo.paa";
		displayName = "RSR Units (Flecktarn)";
		priority = 1;
		side = 1;
	};
	class RSR_faction_tropentarn {
		icon = "\rsr\rsr_core\data\ui\rsr_logo.paa";
		displayName = "RSR Units (Tropentarn)";
		priority = 2;
		side = 1;
	};
};


class CfgVehicles {

	//Fennek
	class I_MRAP_03_F;
	class I_MRAP_03_hmg_F;
	class I_MRAP_03_gmg_F;
	class rsr_fennek_transport_flecktarn : I_MRAP_03_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};
	class rsr_fennek_hmg_flecktarn : I_MRAP_03_hmg_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};
	class rsr_fennek_gmg_flecktarn : I_MRAP_03_gmg_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};
		
	//Bergepanzer
	class B_APC_Tracked_01_CRV_F;
	class rsr_bergepanzer_flecktarn : B_APC_Tracked_01_CRV_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};

	//Panzerhaubitze
	class B_MBT_01_arty_F;
	class rsr_panzerhaubitze_flecktarn : B_MBT_01_arty_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};

	//
	//Vehicles below all hidden ingame and obsolete
	//

	//Wiesent
	class O_Truck_03_transport_F;
	class O_Truck_03_covered_F;
	class O_Truck_03_repair_F;
	class O_Truck_03_ammo_F;
	class O_Truck_03_fuel_F;
	class O_Truck_03_medical_F;
	class rsr_wisent_transport_flecktarn : O_Truck_03_transport_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};
	class rsr_wisent_covered_flecktarn : O_Truck_03_covered_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};
	class rsr_wisent_repair_flecktarn : O_Truck_03_repair_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};
	class rsr_wisent_ammo_flecktarn : O_Truck_03_ammo_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};
	class rsr_wisent_fuel_flecktarn	: O_Truck_03_fuel_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};
	class rsr_wisent_medical_flecktarn : O_Truck_03_medical_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};
	
	//Gepard
	class B_APC_Tracked_01_AA_F;
	class rsr_gepard_flecktarn : B_APC_Tracked_01_AA_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};
	
	//Fuchs
	class I_APC_Wheeled_03_cannon_F;
	class rsr_fuchs_transport_flecktarn : I_APC_Wheeled_03_cannon_F {
		tf_RadioType = "tf_mr3000_bwmod";
	};

	//Boxer
	class CUP_B_M1126_ICV_M2_Desert;
	class CUP_B_M1133_MEV_Desert;
	class rsr_boxer_transport_flecktarn : CUP_B_M1126_ICV_M2_Desert {
		tf_RadioType = "tf_mr3000_bwmod";
	};
	class rsr_boxer_medical_flecktarn : CUP_B_M1133_MEV_Desert {
		tf_RadioType = "tf_mr3000_bwmod";
	};
};